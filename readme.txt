Bienvenue à la formation "Javascript Foundamentals" par Clever Institut
-----------------------------------------------------
Ce dossier comprend :
- 1 support de cours en PDF
- 1 fichier JS avec le code source utilisé dans le support.
- 1 dossier todoApp-begin contenant l'appication Cdiscount app pour les exercices pratiques.
-----------------------------------------------------
Pré-requis pour les exercices.
A l'aide du terminal shell voici les commandes à taper.
cd todoApp-begin/
npm install
npm start
-----------------------------------------------------
Taper l'adresse suivante dans votre navigateur chrome.
http://localhost:3000/
-----------------------------------------------------


